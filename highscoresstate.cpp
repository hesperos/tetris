#include "gamestates.h"
#include "highscoresstate.h"

#include <chrono>
#include <thread>

namespace tetris
{
    HighScoresState::HighScoresState(const std::shared_ptr<Renderer>& renderer) :
        renderer(renderer)
    {
    }

    boost::optional<State::Id> HighScoresState ::enter(const Fsm& fsm)
    {
        renderer->renderText("NOT IMPLEMENTED",
                text::Size::medium,
                text::Style::bold,
                4, 4,
                Color::Red);

        renderer->blit();

        for (;;)
        {
            const boost::optional<Renderer::Input>& input =
                renderer->getKey();

            if (!input)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(250));
                continue;
            }
            else
            {
                break;
            }
        }
        return static_cast<Id>(GameState::Menu);
    }

} /* namespace tetris */
