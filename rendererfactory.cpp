#include "rendererfactory.h"
#include "rendererloader.h"

#include "board.h"
#include "plugin.h"

#include <map>
#include <string>

namespace tetris
{
    std::unique_ptr<Renderer>
        RendererFactory::createRenderer(const std::string& renderer)
        {
            int w = TetrisBoard::Width;
            int h = TetrisBoard::Height;
            return RendererLoader(renderer).instantiate(w, h, 0, 0);
        }

} /* namespace tetris */
