#ifndef _SCORE_H_
#define _SCORE_H_

#include "eventproducer.h"
#include "paintable.h"
#include "types.h"

namespace tetris
{
    class Score
    {
    public:
        virtual ~Score() = default;
        Score();

        virtual int get() const;
        virtual void reset();
        virtual void score(const class Level& level, int linesDeleted);

    protected:
        int score_;
    };

    class LevelListener
    {
    public:
        virtual ~LevelListener() = default;
        virtual void LevelChanged(const class Level& level) = 0;
    };

    class Level :
        public EventProducer<LevelListener>
    {
    public:
        virtual ~Level() = default;
        Level();

        virtual int get() const;
        virtual void reset();
        virtual void up();

    protected:
        int level;
    };

    class GameScore :
        public Paintable<GameScore>
    {
    public:
        GameScore(std::unique_ptr<Level> level,
                std::unique_ptr<Score> score);

        void score(int linesDeleted);
        void reset();
        Speed getSpeed() const;

        Score& getScore() const;
        Level& getLevel() const;

    protected:
        std::unique_ptr<Level> level;
        std::unique_ptr<Score> score_;
        int linesCleared;
    };

} /* namespace tetris */

#endif /* _SCORE_H_ */
