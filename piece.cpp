#include "piece.h"

#include "board.h"
#include "elementpainter.h"

namespace tetris
{
    Piece::Piece(PieceGrid grid,
            BoardPosition position,
            Rotation rotation,
            Speed speed,
            Color color) :
        grid(std::move(grid)),
        position(position),
        speed(speed),
        color(color)
    {
        this->grid.setRotation(rotation);
    }

    void Piece::rotate(Rotation n)
    {
        grid.rotate(n);
    }

    const PieceGrid& Piece::getGrid() const
    {
        return grid;
    }

    const BoardPosition& Piece::getPosition() const
    {
        return position;
    }

    void Piece::setPosition(const BoardPosition& p)
    {
        position = p;
    }

    void Piece::setSpeed(const Speed& speed)
    {
        this->speed = speed <= 0 ? 1 : speed;
    }

    Speed Piece::getSpeed() const
    {
        return speed;
    }
    
    Color Piece::getColor() const
    {
        return color;
    }
} /* namespace tetris */
