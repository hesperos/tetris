#ifndef RENDERERFACTORY_H_
#define RENDERERFACTORY_H_

#include "renderer.h"

#include <memory>

namespace tetris
{
    class RendererFactory
    {
    public:
        std::unique_ptr<Renderer>
            createRenderer(const std::string& renderer);
    };

} /* namespace tetris */

#endif /* RENDERERFACTORY_H_ */
