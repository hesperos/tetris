#include <benchmark/benchmark.h>

#include "../piece.h"
#include "../position.h"

#include <boost/make_unique.hpp>

using namespace tetris;

static void BM_Construction(benchmark::State& state)
{
    while (state.KeepRunning())
    {
        std::unique_ptr<Piece> piece =
            boost::make_unique<Piece>(
                    PieceGrid{""},
                    BoardPosition{},
                    Rotation{},
                    Speed{});
    }
}
BENCHMARK(BM_Construction);

BENCHMARK_MAIN();
