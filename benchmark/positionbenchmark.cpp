#include <benchmark/benchmark.h>

#include "../position.h"

#include <boost/make_unique.hpp>

using namespace tetris;

static void BM_Copy(benchmark::State& state)
{
    std::unique_ptr<BoardPosition> position =
        boost::make_unique<BoardPosition>();

    while (state.KeepRunning())
    {
        BoardPosition posCopy = *position;
    }
}
BENCHMARK(BM_Copy);

BENCHMARK_MAIN();
