#include "config.h"
#include "game.h"
#include "rendererfactory.h"

#include <cstdlib>
#include <memory>

#include <boost/filesystem.hpp>
#include <boost/make_unique.hpp>

int main(int argc, const char *argv[])
{
    const char* home = ::getenv("HOME");
    boost::filesystem::path configPath = "/tmp";
    if (home)
    {
        configPath = home;
    }
    configPath /= ".tetris.xml";
    tetris::XmlConfiguration xmlConfig(configPath.native());

    std::unique_ptr<tetris::RendererFactory> rendererFactory =
        boost::make_unique<tetris::RendererFactory>();

    std::shared_ptr<tetris::Renderer> renderer =
        rendererFactory->createRenderer(xmlConfig.getRenderer());

    std::unique_ptr<tetris::Game> game =
        boost::make_unique<tetris::Game>(renderer);

    int rv = game->run();
    xmlConfig.save();
    return rv;
}
