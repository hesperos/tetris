#include "pluginloader.h"

namespace tetris
{
    PluginLoader::PluginLoader(const std::string& pluginName) :
        pluginLibrary(pluginName)
    {
    }

} /* namespace tetris */

