#ifndef CONFIG_H_
#define CONFIG_H_

#include <string>

namespace tetris
{
    class Configuration
    {
    public:
        virtual ~Configuration() = default;

        virtual std::string getRenderer() const = 0;
        virtual void save() = 0;

    };


    class XmlConfiguration :
        public Configuration
    {
    public:
        using Version = int;
        XmlConfiguration(const std::string& fileName);

        std::string getRenderer() const;
        void save();

    private:
        static const Version version;
        const std::string filePath;
        struct Settings
        {
            Settings();
            std::string renderer;
        } settings;

        void initialize();
    };

} /* namespace tetris */

#endif /* CONFIG_H_ */
