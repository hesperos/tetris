#ifndef RENDERER_NCURSES_H_
#define RENDERER_NCURSES_H_

#include "../../plugin.h"
#include "../../renderer.h"

#include <curses.h>

#include <memory>

namespace tetris
{
    class RendererNcurses :
        public Renderer,
        public Plugin
    {
    public:
        RendererNcurses(int w, int h, int offsetX, int offsetY);
        ~RendererNcurses();

        boost::optional<Input> getKey() const override;
        void renderBlock(size_t x, size_t y, Color c) override;
        void renderBorder() override;
        void renderText(const std::string& str,
                text::Size size,
                text::Style style,
                size_t x,
                size_t y,
                Color c) override;
        void blit() override;
        void clear() override;

    private:
        const int width;
        const int height;
        const bool isInitialised;
        std::unique_ptr<::WINDOW, decltype(&::delwin)> board;

        bool initialise();
        Input mapKeyToInput(int key) const;
    };
} /* namespace tetris */

extern "C" std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy);

#endif /* RENDERER_NCURSES_H_ */
