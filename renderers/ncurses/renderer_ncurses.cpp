#include "../../constants.h"
#include "renderer_ncurses.h"

#include <string>

#include <boost/make_unique.hpp>

namespace
{
    inline constexpr tetris::text::Style
        operator&(tetris::text::Style a, tetris::text::Style b)
    {
        return static_cast<tetris::text::Style>(
                static_cast<int>(a) & static_cast<int>(b));
    }
}

namespace tetris
{
    RendererNcurses::RendererNcurses(int w, int h,
            int offsetX, int offsetY) :
        width(w),
        height(h),
        isInitialised(initialise()),
        board(::newwin(h + HorizontalBorders, w + VerticalBorders,
                    offsetX, offsetY), &::delwin)
    {
    }

    bool RendererNcurses::initialise()
    {
        ::initscr();

        if (::has_colors())
        {
            ::start_color();
        }

        init_pair(static_cast<int>(Color::Red), COLOR_BLACK, COLOR_RED);
        init_pair(static_cast<int>(Color::Green), COLOR_BLACK, COLOR_GREEN);
        init_pair(static_cast<int>(Color::Blue), COLOR_BLACK, COLOR_BLUE);
        init_pair(static_cast<int>(Color::Yellow), COLOR_BLACK, COLOR_YELLOW);
        init_pair(static_cast<int>(Color::Magenta), COLOR_BLACK, COLOR_MAGENTA);
        init_pair(static_cast<int>(Color::Cyan), COLOR_BLACK, COLOR_CYAN);
        init_pair(static_cast<int>(Color::White), COLOR_BLACK, COLOR_WHITE);

        ::cbreak();
        ::noecho();

        ::timeout(5);
        ::keypad(stdscr, TRUE);
        ::curs_set(FALSE);

        return true;
    }

    RendererNcurses::~RendererNcurses()
    {
        ::endwin();
    }

    boost::optional<Renderer::Input> RendererNcurses::getKey() const
    {
        int k = ::wgetch(stdscr);
        return k == ERR ? boost::none : boost::optional<Renderer::Input>(
                mapKeyToInput(k));
    }

    Renderer::Input RendererNcurses::mapKeyToInput(int key) const
    {
        Renderer::Input input;
        switch(key)
        {
            case 'q':
                input = Renderer::Input::Quit;
                break;

            case KEY_LEFT:
                input = Renderer::Input::Left;
                break;

            case KEY_RIGHT:
                input = Renderer::Input::Right;
                break;

            case KEY_UP:
                input = Renderer::Input::Up;
                break;

            case KEY_DOWN:
                input = Renderer::Input::Down;
                break;

            case KEY_ENTER:
            case 10:
                input = Renderer::Input::Enter;
                break;
        }
        return input;
    }

    void RendererNcurses::renderBlock(size_t x, size_t y, Color c)
    {
		WINDOW* const w = board.get();
        ::wmove(w, y + HorizontalBorder, x + VerticalBorder);

        ::wattron(w, COLOR_PAIR(static_cast<int>(c)));

        if (Color::None == c)
        {
            ::waddch(w, ' ');
        }
        else
        {
            ::waddch(w, 'x');
        }

        ::wattroff(w, COLOR_PAIR(static_cast<int>(c)));
    }

    void RendererNcurses::renderText(const std::string& str,
            text::Size,
            text::Style style,
            size_t x,
            size_t y,
            Color c)
    {
        int attributes = COLOR_PAIR(static_cast<int>(c));

        if (text::Style::bold == (style & text::Style::bold))
        {
            attributes |= A_BOLD;
        }

        if (text::Style::italic == (style & text::Style::italic))
        {
            attributes |= A_UNDERLINE;
        }

        ::wattron(stdscr, attributes);
        ::mvprintw(y, x, "%s", str.c_str());
        ::wattroff(stdscr, attributes);
    }

    void RendererNcurses::blit()
    {
        ::refresh();
        ::wrefresh(board.get());
    }

    void RendererNcurses::renderBorder()
    {
        ::wborder(board.get(), '|', '|', '-', '-', '+', '+', '+', '+');
    }

    void RendererNcurses::clear()
    {
        ::wclear(board.get());
        ::clear();
    }
} /* namespace tetris */

std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy)
{
    return boost::make_unique<tetris::RendererNcurses>(w, h, ox, oy);
}
