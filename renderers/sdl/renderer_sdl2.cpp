#include "../../constants.h"
#include "renderer_sdl2.h"
#include <paths.h>

#include <boost/make_unique.hpp>

namespace tetris
{
    const int RendererSdl2::Width = 500;
    const int RendererSdl2::Height = 400;
    const int RendererSdl2::tileSize = 20;

    RendererSdl2::RendererSdl2(
            const std::string& fontPath,
            int w, int h, int offsetX, int offsetY) :
        width(w),
        height(h),
        offsetX(offsetX),
        offsetY(offsetY),
        mainWindow(nullptr, &SDL_DestroyWindow),
        renderer(nullptr, &SDL_DestroyRenderer),
        fontSmall(nullptr, &TTF_CloseFont),
        fontNormal(nullptr, &TTF_CloseFont),
        fontMedium(nullptr, &TTF_CloseFont),
        fontBig(nullptr, &TTF_CloseFont)
    {
        SDL_Init(SDL_INIT_VIDEO);
        TTF_Init();

        mainWindow.reset(SDL_CreateWindow("Tetris",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                Width,
                Height,
                SDL_WINDOW_SHOWN));

        renderer.reset(SDL_CreateRenderer(mainWindow.get(), -1,
                SDL_RENDERER_ACCELERATED));

        fontSmall.reset(TTF_OpenFont(fontPath.c_str(), 12));
        fontNormal.reset(TTF_OpenFont(fontPath.c_str(), 16));
        fontMedium.reset(TTF_OpenFont(fontPath.c_str(), 24));
        fontBig.reset(TTF_OpenFont(fontPath.c_str(), 32));

        if (!fontSmall || !fontNormal || !fontMedium || !fontBig)
        {
            throw std::runtime_error("Unable to load fonts");
        }
    }

    RendererSdl2::~RendererSdl2()
    {
        fontSmall.reset();
        fontNormal.reset();
        fontMedium.reset();
        fontBig.reset();
        renderer.reset();
        mainWindow.reset();
        TTF_Quit();
        SDL_Quit();
    }

    boost::optional<Renderer::Input> RendererSdl2::getKey() const
    {
        SDL_Event event;
        if (!SDL_WaitEventTimeout(&event, 5))
        {
            return boost::none;
        }

        switch(event.type)
        {
            case SDL_QUIT:
                return Renderer::Input::Quit;
                break;

            case SDL_KEYDOWN:
                switch(event.key.keysym.sym)
                {
                    case SDLK_q:
                        return Renderer::Input::Quit;
                        break;

                    case SDLK_LEFT:
                        return Renderer::Input::Left;
                        break;

                    case SDLK_RIGHT:
                        return Renderer::Input::Right;
                        break;

                    case SDLK_UP:
                        return Renderer::Input::Up;
                        break;

                    case SDLK_DOWN:
                        return Renderer::Input::Down;
                        break;

                    case SDLK_RETURN:
                    case SDLK_KP_ENTER:
                        return Renderer::Input::Enter;
                        break;
                }
                break;
        }

        return boost::none;
    }

    void RendererSdl2::renderBorder()
    {
        SDL_Rect rect { offsetX*tileSize,
                offsetY*tileSize,
                tileSize * (width + offsetX) + HorizontalBorders,
                tileSize * (height + offsetY) + VerticalBorders};
        SDL_SetRenderDrawColor(renderer.get(), 0xff, 0xff, 0xff, 0xff);
        SDL_RenderDrawRect(renderer.get(), &rect);
    }

    void RendererSdl2::renderBlock(size_t x, size_t y, Color c)
    {
        SDL_Rect rect {
            HorizontalBorder + tileSize * static_cast<int>(x + offsetX),
            VerticalBorder + tileSize * static_cast<int>(y + offsetY),
            tileSize,
            tileSize
        };

        SDL_Color color = makeColor(c);
        SDL_SetRenderDrawColor(renderer.get(), color.r, color.g, color.b, color.a);
        SDL_RenderFillRect(renderer.get(), &rect);

        color = makeBorderColor(c);
        SDL_SetRenderDrawColor(renderer.get(), color.r, color.g, color.b, color.a);
        SDL_RenderDrawRect(renderer.get(), &rect);
    }


    SDL_Color RendererSdl2::makeBorderColor(Color c)
    {
        const uint8_t m = 0xaa;
        switch(c)
        {
            case Color::None:
                return SDL_Color {0, 0, 0, 0xff};
                break;
            case Color::Red:
                return SDL_Color {m, 0, 0, 0xaa};
                break;
            case Color::Green:
                return SDL_Color {0, m, 0, 0xaa};
                break;
            case Color::Blue:
                return SDL_Color {0, 0, m, 0xaa};
                break;
            case Color::Yellow:
                return SDL_Color {m, m, 0, 0xaa};
                break;
            case Color::Magenta:
                return SDL_Color {m, 0, m, 0xaa};
                break;
            case Color::Cyan:
                return SDL_Color {0, m, m, 0xaa};
                break;
            case Color::White:
                return SDL_Color {m, m, m, 0xaa};
                break;
        }

        return SDL_Color{ 0, 0, 0, 0xff };
    }

    SDL_Color RendererSdl2::makeColor(Color c)
    {
        const uint8_t m = 0xff;
        switch(c)
        {
            case Color::None:
                return SDL_Color {0, 0, 0, 0xff};
                break;
            case Color::Red:
                return SDL_Color {m, 0, 0, 0xff};
                break;
            case Color::Green:
                return SDL_Color {0, m, 0, 0xff};
                break;
            case Color::Blue:
                return SDL_Color {0, 0, m, 0xff};
                break;
            case Color::Yellow:
                return SDL_Color {m, m, 0, 0xff};
                break;
            case Color::Magenta:
                return SDL_Color {m, 0, m, 0xff};
                break;
            case Color::Cyan:
                return SDL_Color {0, m, m, 0xff};
                break;
            case Color::White:
                return SDL_Color {m, m, m, 0xff};
                break;
        }

        return SDL_Color{ 0, 0, 0, 0xff };
    }

    void RendererSdl2::renderText(const std::string& str,
            text::Size size,
            text::Style style,
            size_t x,
            size_t y,
            Color c)
    {
        TTF_Font* font = fontNormal.get();
        switch (size)
        {
            case text::Size::small:
                font = fontSmall.get();
                break;
            case text::Size::normal:
                font = fontNormal.get();
                break;
            case text::Size::medium:
                font = fontMedium.get();
                break;
            case text::Size::big:
                font = fontBig.get();
                break;
        }

        SDL_Surface* textSurface = TTF_RenderText_Solid(font,
                str.c_str(), makeColor(c));

        SDL_Texture* text =
            SDL_CreateTextureFromSurface(renderer.get(), textSurface);

        SDL_FreeSurface(textSurface);
        SDL_Rect renderArea = {
            tileSize * static_cast<int>(x),
            (tileSize + textSurface->h) * static_cast<int>(y),
            textSurface->w,
            textSurface->h
        };

        SDL_RenderCopy(renderer.get(), text, NULL, &renderArea);
        SDL_DestroyTexture(text);
    }

    void RendererSdl2::blit()
    {
        SDL_RenderPresent(renderer.get());
    }

    void RendererSdl2::clear()
    {
        SDL_SetRenderDrawColor(renderer.get(), 0x00, 0x00, 0x00, 0xff);
        SDL_RenderClear(renderer.get());
    }

} /* namespace tetris */

std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy)
{
    const std::string fontPath =
        std::string(tetris::prefix) +
        "/share/tetris/fonts/verdana.ttf";

    return boost::make_unique<tetris::RendererSdl2>(
            fontPath, w, h, ox, oy);
}
