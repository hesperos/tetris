#ifndef RENDERER_SDL2_H_
#define RENDERER_SDL2_H_

#include "../../plugin.h"
#include "../../renderer.h"

#include <SDL.h>
#include <SDL_ttf.h>

#include <memory>

namespace tetris
{
    class RendererSdl2 :
        public Renderer,
        public Plugin
    {
    public:
        RendererSdl2(const std::string& fontPath,
                int w, int h, int offsetX, int offsetY);
        ~RendererSdl2();

        boost::optional<Input> getKey() const override;
        void renderBlock(size_t x, size_t y, Color c) override;
        void renderBorder() override;
        void renderText(const std::string& str,
                text::Size size,
                text::Style style,
                size_t x,
                size_t y,
                Color c) override;
        void blit() override;
        void clear() override;

    private:
        static const int Width;
        static const int Height;
        static const int tileSize;

        const int width;
        const int height;
        const int offsetX;
        const int offsetY;

        std::unique_ptr<SDL_Window, decltype(&SDL_DestroyWindow)> mainWindow;
        std::unique_ptr<SDL_Renderer, decltype(&SDL_DestroyRenderer)> renderer;

        // fonts
        std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> fontSmall;
        std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> fontNormal;
        std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> fontMedium;
        std::unique_ptr<TTF_Font, decltype(&TTF_CloseFont)> fontBig;

        static SDL_Color makeColor(Color c);
        static SDL_Color makeBorderColor(Color c);
    };
} /* namespace tetris */

extern "C" std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy);

#endif /* RENDERER_SDL2_H_ */
