#include "renderer_null.h"

#include <boost/make_unique.hpp>

namespace tetris
{

    RendererNull::RendererNull(int, int, int, int) :
        blitCounter(0),
        clearCounter(0)
    {
    }

    boost::optional<Renderer::Input> RendererNull::getKey() const
    {
        return blitCounter > 5 ?
            boost::optional<Input>(Input::Quit) : boost::none;
    }

    void RendererNull::renderBlock(size_t, size_t, Color)
    {
    }

    void RendererNull::renderBorder()
    {
    }

    void RendererNull::renderText(const std::string&,
            text::Size,
            text::Style,
            size_t,
            size_t,
            Color)
    {
    }

    void RendererNull::blit()
    {
        blitCounter++;
    }

    void RendererNull::clear()
    {
        clearCounter++;
    }
} /* namespace tetris */

std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy)
{
    return boost::make_unique<tetris::RendererNull>(w, h, ox, oy);
}
