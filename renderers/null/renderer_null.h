#ifndef RENDERER_NULL_H_
#define RENDERER_NULL_H_

#include "../../plugin.h"
#include "../../renderer.h"

#include <memory>

namespace tetris
{
    class RendererNull :
        public Renderer,
        public Plugin
    {
    public:
        RendererNull(int w, int h, int offsetX, int offsetY);

        boost::optional<Input> getKey() const override;
        void renderBlock(size_t x, size_t y, Color c) override;
        void renderBorder() override;
        void renderText(const std::string& str,
                text::Size size,
                text::Style style,
                size_t x, size_t y,
                Color c) override;
        void blit() override;
        void clear() override;

    private:
        int blitCounter;
        int clearCounter;
    };
} /* namespace tetris */

extern "C" std::unique_ptr<tetris::Plugin>
create(int w, int h, int ox, int oy);

#endif /* RENDERER_NULL_H_ */
