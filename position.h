#ifndef POSITION_H_
#define POSITION_H_

#include <cstdint>

namespace tetris
{
    class BoardPosition
    {
    public:
        BoardPosition(uint8_t x = 0,
                uint8_t y = 0);

        BoardPosition& operator<<(uint8_t n);
        BoardPosition& operator>>(uint8_t n);
        BoardPosition& operator+(uint8_t n);
        BoardPosition& operator-(uint8_t n);

        int8_t getX() const;
        int8_t getY() const;

    protected:
        struct
        {
            int16_t x : 8;
            int16_t y : 8;
        } position;
    };

} /* namespace tetris */

#endif /* POSITION_H_ */
