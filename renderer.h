#ifndef RENDERER_H_
#define RENDERER_H_

#include "color.h"

#include <cstddef>

#include <boost/optional.hpp>

namespace tetris
{
    namespace text
    {
        enum class Size : int
        {
            small,
            normal,
            medium,
            big
        };

        enum class Style : int
        {
            normal = (1 << 0),
            bold = (1 << 1),
            italic = (1 << 2),
        };
    } // namespace text

    class Renderer
    {
    public:
        enum class Input : int
        {
            Quit,
            Left,
            Right,
            Down,
            Up,
            Enter,
        };

        virtual ~Renderer() = default;

        virtual boost::optional<Input> getKey() const = 0;
        virtual void renderBlock(size_t x, size_t y, Color c) = 0;
        virtual void renderBorder() = 0;
        virtual void renderText(const std::string& str,
                text::Size size,
                text::Style style,
                size_t x, size_t y,
                Color c) = 0;
        virtual void blit() = 0;
        virtual void clear() = 0;
    };
} /* namespace tetris */

#endif /* RENDERER_H_ */
