#ifndef RENDERERLOADER_H_
#define RENDERERLOADER_H_

#include "pluginloader.h"
#include "renderer.h"

namespace tetris
{
    class RendererLoader
    {
    public:
        RendererLoader(const std::string& rendererName);

        std::unique_ptr<Renderer> instantiate(int w, int h, int ox, int oy) const
        {
            return std::unique_ptr<Renderer>(
                    dynamic_cast<Renderer*>(pluginLoader.instantiate(
                            w, h, ox, oy).release()));
        }

    protected:
        PluginLoader pluginLoader;
    };
} /* namespace tetris */



#endif /* RENDERERLOADER_H_ */
