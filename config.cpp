#include "config.h"

#include <boost/filesystem.hpp>
#include <boost/property_tree/exceptions.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace tetris
{
    const XmlConfiguration::Version XmlConfiguration::version = 1;

    XmlConfiguration::Settings::Settings() :
        renderer("sdl2")
    {
    }

    XmlConfiguration::XmlConfiguration(const std::string& filePath) :
        filePath(filePath)
    {
        initialize();
    }

    void XmlConfiguration::initialize()
    {
        if (!boost::filesystem::exists(filePath))
        {
            return;
        }

        try
        {
            boost::property_tree::ptree propertyTree;
            boost::property_tree::read_xml(filePath, propertyTree);
            if (version == propertyTree.get<Version>("version"))
            {
                settings.renderer = propertyTree.get<std::string>("renderer.renderer");
            }
        }
        catch(const boost::property_tree::ptree_error&)
        {
        }
    }

    void XmlConfiguration::save()
    {
        boost::property_tree::ptree propertyTree;
        propertyTree.put("version", version);
        propertyTree.put("renderer.renderer", settings.renderer);
        boost::property_tree::write_xml(filePath, propertyTree);
    }

    std::string XmlConfiguration::getRenderer() const
    {
        return settings.renderer;
    }
} /* namespace tetris */

