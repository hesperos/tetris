#ifndef HIGHSCORESSTATE_H_
#define HIGHSCORESSTATE_H_

#include "fsm.h"
#include "renderer.h"

namespace tetris
{
    class HighScoresState :
        public State
    {
    public:
        HighScoresState(const std::shared_ptr<Renderer>& renderer);
        boost::optional<Id> enter(const Fsm& fsm) override;

    private:
        std::shared_ptr<Renderer> renderer;
    };

} /* namespace tetris */

#endif /* HIGHSCORESSTATE_H_ */
