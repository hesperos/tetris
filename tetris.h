#ifndef TETRIS_H_
#define TETRIS_H_

#include "elementpainter.h"
#include "score.h"
#include "piecefactory.h"

#include <memory>

namespace tetris
{
    class Tetris
    {
    public:
        Tetris(std::unique_ptr<TetrisBoard> board,
                std::unique_ptr<PieceFactory> pf,
                std::unique_ptr<ElementPainter> ep,
                std::unique_ptr<GameScore> score,
                const std::shared_ptr<Renderer>& renderer);

        void step();

        bool isRunning() const;

    protected:
        std::unique_ptr<TetrisBoard> board;
        std::unique_ptr<PieceFactory> pieceFactory;
        std::unique_ptr<ElementPainter> elementPainter;
        std::unique_ptr<GameScore> score;
        std::shared_ptr<Renderer> renderer;

        bool isRunning_;
        bool isPieceRepaintRequired;
        bool isBoardRepaintRequired;
        bool isClearBufferNeeded;
        unsigned frameCounter;

        std::unique_ptr<Piece> currentPiece;

        bool adjustPieceToBoard();
        void processInput();
        void progressPiece();
        void render();

        bool detectCollisions();
        bool isForbidden();
    };
} /* namespace tetris */

#endif /* TETRIS_H_ */
