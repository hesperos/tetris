#ifndef FSM_H_
#define FSM_H_

#include <memory>

#include <boost/optional.hpp>

namespace tetris
{
    class State
    {
    public:
        using Id = int;

        virtual ~State() = default;
        virtual boost::optional<int> enter(const class Fsm& fsm) = 0;
    };

    class Fsm
    {
    public:
        virtual ~Fsm() = default;

        Fsm(State::Id initial);
        virtual State::Id run();

    protected:
        State::Id currentState;
        int transitions;

        virtual std::unique_ptr<State> makeState(State::Id stateId) = 0;
    };

} /* namespace tetris */

#endif /* FSM_H_ */
