#ifndef COMMAND_H_
#define COMMAND_H_

namespace tetris
{
    class Command
    {
    public:
        virtual ~Command() = default;

        virtual void execute() const = 0;
        virtual void undo() const = 0;
    };
} /* namespace tetris */

#endif /* COMMAND_H_ */
