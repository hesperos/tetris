#include "gamestates.h"
#include "menustate.h"

#include <chrono>
#include <thread>

namespace tetris
{
    MenuState::MenuState(const std::shared_ptr<Renderer>& renderer) :
        selected{0},
        options{"Play", "High scores", "Quit"},
        renderer(renderer)
    {
    }

    boost::optional<State::Id> MenuState::enter(const Fsm& fsm)
    {
        for (;;)
        {
            render();
            const boost::optional<Renderer::Input>& input =
                renderer->getKey();

            if (!input)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                continue;
            }

            switch(*input)
            {
                case Renderer::Input::Quit:
                    return boost::none;
                    break;

                case Renderer::Input::Up:
                    if (selected)
                        selected--;
                    else
                        selected = options.size() - 1;
                    break;

                case Renderer::Input::Down:
                    selected++;
                    break;

                case Renderer::Input::Enter:
                    return transition();
                    break;

                default:
                    break;
            } // switch

            selected = selected % options.size();
        }

        return boost::none;
    }

    boost::optional<State::Id> MenuState::transition()
    {
        switch(selected)
        {
            case 0:
                return static_cast<Id>(GameState::Game);
                break;

            case 1:
                return static_cast<Id>(GameState::HighScores);
                break;

            case 2:
                return boost::none;
                break;

            default:
                break;
        }

        return boost::none;
    }

    void MenuState::render() const
    {
        renderer->renderText("-- Tetris --",
                text::Size::big,
                text::Style::bold,
                5, 1,
                Color::White);

        size_t offset = 3;

        for (size_t index = 0; index < options.size(); index++)
        {
            renderer->renderText(options[index],
                    text::Size::medium,
                    text::Style::bold,
                    4, index + offset,
                    index == selected ? Color::Red : Color::White);
        }

        renderer->blit();
    }

} /* namespace tetris */
