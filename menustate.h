#ifndef MENUSTATE_H_
#define MENUSTATE_H_

#include "fsm.h"
#include "renderer.h"

#include <array>
#include <memory>
#include <string>

namespace tetris
{
    class MenuState :
        public State
    {
    public:
        MenuState(const std::shared_ptr<Renderer>& renderer);
        boost::optional<Id> enter(const Fsm& fsm) override;

    protected:
        size_t selected;
        const std::array<std::string, 3> options;
        std::shared_ptr<Renderer> renderer;

        void render() const;
        boost::optional<Id> transition();
    };

} /* namespace tetris */

#endif /* MENUSTATE_H_ */
