#ifndef PAINTABLE_H_
#define PAINTABLE_H_

#include "elementpainter.h"

namespace tetris
{
    template <typename ConcretePaintableT>
    class Paintable
    {
    public:
        virtual ~Paintable() = default;

        void paint(ElementPainter& ep)
        {
            ep.paint(static_cast<ConcretePaintableT&>(*this));
        }
    };
} /* namespace tetris */

#endif /* PAINTABLE_H_ */
