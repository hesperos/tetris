#ifndef ELEMENTPAINTER_H_
#define ELEMENTPAINTER_H_

#include "constants.h"
#include "renderer.h"

#include <memory>

namespace tetris
{
    // forward declarations
    class Piece;
    class GameScore;
    template <size_t, size_t> class Board;

    // aliases
    using TetrisBoard = Board<BoardWidth, BoardHeight>;

    class ElementPainter
    {
    public:
        ~ElementPainter();
        ElementPainter(const std::shared_ptr<Renderer>& renderer);

        void paint(Piece& piece);
        void paint(TetrisBoard& board);
        void paint(GameScore& score);

        void blit();
        void clear();

    private:
        std::shared_ptr<Renderer> renderer;

        class PiecePaint;
        std::unique_ptr<PiecePaint> piecePaintCommand;
    };
} /* namespace tetris */

#endif /* ELEMENTPAINTER_H_ */
