#ifndef PIECEFACTORY_H_
#define PIECEFACTORY_H_

#include "piece.h"

#include <memory>
#include <vector>
#include <random>

namespace tetris
{
    class PieceFactory
    {
    public:
        PieceFactory();
        std::unique_ptr<Piece> createRandom(const Speed& speed);

    private:
        static std::vector<PieceGrid> grids;

        std::random_device randomDevice;
        std::mt19937 randomGenerator;

        std::uniform_int_distribution<size_t> randomGrid;
        std::uniform_int_distribution<size_t> randomRotation;
        std::uniform_int_distribution<size_t> randomColor;
        std::uniform_int_distribution<size_t> randomHorizontalPos;
    };

} /* namespace tetris */

#endif /* PIECEFACTORY_H_ */
