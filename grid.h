#ifndef GRID_H_
#define GRID_H_

#include "types.h"

#include <bitset>

namespace tetris
{
    template <Dimension WidthV, Dimension HeightV, Rotation RotationsV>
        class Grid
        {
        public:
            constexpr static Dimension Width = WidthV;
            constexpr static Dimension Height = HeightV;
            constexpr static Rotation Rotations = RotationsV;

            constexpr Grid(const std::string& gridStr,
                    Rotation rotation = 0) :
                grid(gridStr),
                rotation(rotation)
            {
            }

            void setRotation(Rotation r)
            {
                rotation = r % Rotations;
            }

            Rotation getRotation() const
            {
                return rotation;
            }

            void rotate(int n = 1)
            {
                rotation = (rotation + n) % Rotations;
            }

            size_t index(size_t x, size_t y) const
            {
                return x + (y * Width) + (rotation * Width * Height);
            }

            bool operator()(Dimension x, Dimension y) const
            {
                return grid.test(index(x, y));
            }

        private:
            const std::bitset<Width * Height * Rotations> grid;
            Rotation rotation;
        };
} /* namespace tetris */

#endif /* GRID_H_ */
