#include "elementpainter.h"

#include "board.h"
#include "command.h"
#include "constants.h"
#include "piece.h"
#include "score.h"

#include <iomanip>
#include <sstream>

#include <boost/make_unique.hpp>

namespace tetris
{

    class ElementPainter::PiecePaint :
        public tetris::Command
    {
    public:
        PiecePaint(const std::shared_ptr<tetris::Renderer>& renderer,
                const tetris::Piece& piece);

        void execute() const override;
        void undo() const override;

    protected:
        std::shared_ptr<tetris::Renderer> renderer;
        const tetris::Piece piece;

        void paint(tetris::Color color) const;
    };

    ElementPainter::PiecePaint::PiecePaint(
            const std::shared_ptr<tetris::Renderer>& renderer,
            const tetris::Piece& piece) :
        renderer(renderer),
        piece(piece)
    {
    }

    void ElementPainter::PiecePaint::paint(tetris::Color color) const
    {
        const tetris::PieceGrid& grid = piece.getGrid();
        const tetris::BoardPosition& pos = piece.getPosition();

        for (size_t i = 0; i < tetris::PieceGrid::Width; i++)
        {
            for (size_t j = 0; j < tetris::PieceGrid::Height; j++)
            {
                if (grid(i,j))
                    renderer->renderBlock(i + pos.getX(),
                            j + pos.getY(),
                            color);
            }
        }
    }

    void ElementPainter::PiecePaint::execute() const
    {
        paint(piece.getColor());
    }

    void ElementPainter::PiecePaint::undo() const
    {
        paint(tetris::Color::None);
    }

    ElementPainter::ElementPainter(
            const std::shared_ptr<Renderer>& renderer) :
        renderer(renderer)
    {
    }

    ElementPainter::~ElementPainter()
    {
    }

    void ElementPainter::paint(Piece& piece)
    {
        if (piecePaintCommand)
        {
            piecePaintCommand->undo();
        }

        piecePaintCommand = boost::make_unique<PiecePaint>(renderer, piece);
        piecePaintCommand->execute();
    }

    void ElementPainter::paint(GameScore& score)
    {
        std::stringstream ss;
        renderer->renderText("Score",
                text::Size::normal,
                text::Style::normal,
                BoardWidth + HorizontalBorders + 2,
                2,
                Color::White);

        ss << std::setw(5) << score.getScore().get();
        renderer->renderText(ss.str(),
                text::Size::normal,
                text::Style::normal,
                BoardWidth + HorizontalBorders + 2,
                3,
                Color::White);

        renderer->renderText("Level",
                text::Size::normal,
                text::Style::normal,
                BoardWidth + HorizontalBorders + 2,
                5,
                Color::White);

        ss.str(std::string());
        ss << std::setw(5) << score.getLevel().get();
        renderer->renderText(ss.str(),
                text::Size::normal,
                text::Style::normal,
                BoardWidth + HorizontalBorders + 2,
                6,
                Color::White);
    }

    void ElementPainter::paint(TetrisBoard& board)
    {
        for (size_t i = 0; i < TetrisBoard::Width; i++)
        {
            for (size_t j = 0; j < TetrisBoard::Height; j++)
            {
                Color c = board.index(i, j);
                if (c != Color::None)
                    renderer->renderBlock(i, j, c);
            }
        }

        renderer->renderBorder();
        piecePaintCommand.reset();
    }

    void ElementPainter::blit()
    {
        renderer->blit();
    }

    void ElementPainter::clear()
    {
        renderer->clear();
    }

} /* namespace tetris */
