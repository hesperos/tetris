#include "timer.h"

#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>

namespace tetris
{
    class Timer::TimerImpl
    {
    public:
        TimerImpl(const boost::posix_time::time_duration& d,
                std::function<void()> callback) :
            duration(d),
            timer(io, duration),
            callback(callback)
        {
            schedule();
        }

        ~TimerImpl()
        {
            stop();
        }

        void stop()
        {
            io.stop();
        }

        void ioRunner()
        {
            boost::asio::io_service::work work{io};
            io.run();
        }

    private:
        void onExpiry()
        {
            callback();
            timer.expires_at(timer.expires_at() + duration);
            schedule();
        }

        void schedule()
        {
            timer.async_wait(std::bind(&Timer::TimerImpl::onExpiry, this));
        }

        boost::asio::io_service io;
        boost::posix_time::time_duration duration;
        boost::asio::deadline_timer timer;
        std::function<void()> callback;
    };

    Timer::Timer(boost::posix_time::time_duration d,
            std::function<void()> callback) :
        pImpl(std::make_shared<TimerImpl>(d, callback)),
        ioThread(std::bind(&Timer::TimerImpl::ioRunner, pImpl))
    {
    }

    Timer::~Timer()
    {
        stop();
        pImpl.reset();
        ioThread.join();
    }

    void Timer::stop()
    {
        pImpl->stop();
    }

} /* namespace tetris */
