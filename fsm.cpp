#include "fsm.h"

#include <iostream>

namespace tetris
{
    Fsm::Fsm(State::Id initial) :
        currentState(initial),
        transitions(0)
    {
    }

    int Fsm::run()
    {
        try
        {
            for(;;)
            {
                std::unique_ptr<State> state = makeState(currentState);
                if (!state)
                {
                    break;
                }

                boost::optional<State::Id> newState = state->enter(*this);
                if (!newState)
                {
                    break;
                }

                currentState = *newState;
                transitions++;
            }

            return 0;
        }
        catch(const std::exception& e)
        {
            std::cerr << "Caught exception: " << e.what() << std::endl;
        }
        catch(...)
        {
            std::cerr << "Unknown exception" << std::endl;
        }

        return -1;
    }
} /* namespace tetris */
