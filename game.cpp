#include "game.h"
#include "gamestates.h"

// states
#include "highscoresstate.h"
#include "menustate.h"
#include "tetrisstate.h"

#include <boost/make_unique.hpp>

namespace tetris
{
    Game::Game(const std::shared_ptr<Renderer>& renderer) :
        Fsm(static_cast<int>(GameState::Menu)), // TODO fix this to menu
        renderer(renderer)
    {
    }

    std::unique_ptr<State> Game::makeState(int state)
    {
        GameState gs = static_cast<GameState>(state);
        switch (gs)
        {
            case GameState::Menu:
                return createMenuState();
                break;

            case GameState::Game:
                return createTetrisState();
                break;

            case GameState::HighScores:
                return createHighScoresState();
                break;

            default:
                 break;
        }

        return std::unique_ptr<State>();
    }

    std::unique_ptr<State> Game::createMenuState()
    {
        renderer->clear();
        return boost::make_unique<MenuState>(renderer);
    }

    std::unique_ptr<State> Game::createTetrisState()
    {
        std::unique_ptr<tetris::TetrisBoard> board =
            boost::make_unique<tetris::TetrisBoard>();

        std::unique_ptr<tetris::PieceFactory> pf =
            boost::make_unique<tetris::PieceFactory>();

        std::unique_ptr<tetris::ElementPainter> ep =
            boost::make_unique<tetris::ElementPainter>(renderer);

        std::unique_ptr<tetris::GameScore> score =
            boost::make_unique<tetris::GameScore>(
                    boost::make_unique<tetris::Level>(),
                    boost::make_unique<tetris::Score>());

        const boost::posix_time::milliseconds refreshRate(16);

        renderer->clear();
        return boost::make_unique<TetrisState>(std::move(board),
                std::move(pf),
                std::move(ep),
                std::move(score),
                std::move(renderer),
                refreshRate);
    }

    std::unique_ptr<State> Game::createHighScoresState()
    {
        renderer->clear();
        return boost::make_unique<HighScoresState>(renderer);
    }
} /* namespace tetris */
