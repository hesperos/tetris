#include "piecetransformations.h"

#include <algorithm>

namespace tetris
{
    PieceTransformation::PieceTransformation(Piece& piece) :
        piece(piece)
    {
    }

    PieceRotate::PieceRotate(Piece& piece,
            int rotations) :
        PieceTransformation(piece),
        rotations(rotations)
    {
    }

    void PieceRotate::execute() const
    {
        piece.rotate(rotations);
    }

    void PieceRotate::undo() const
    {
        piece.rotate(-1 * rotations);
    }

    PieceShiftLeft::PieceShiftLeft(Piece& piece, size_t offset) :
        PieceTransformation(piece),
        offset(offset)
    {
    }

    void PieceShiftLeft::execute() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p << offset);
    }

    void PieceShiftLeft::undo() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p >> offset);
    }

    PieceShiftRight::PieceShiftRight(Piece& piece, size_t offset) :
        PieceTransformation(piece),
        offset(offset)
    {
    }

    void PieceShiftRight::execute() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p >> offset);
    }

    void PieceShiftRight::undo() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p << offset);
    }

    PieceMoveDown::PieceMoveDown(Piece& piece, size_t offset) :
        PieceTransformation(piece),
        offset(offset)
    {
    }

    void PieceMoveDown::execute() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p + offset);
    }

    void PieceMoveDown::undo() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p - offset);
    }

    PieceMoveUp::PieceMoveUp(Piece& piece, size_t offset) :
        PieceTransformation(piece),
        offset(offset)
    {
    }

    void PieceMoveUp::execute() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p - offset);
    }

    void PieceMoveUp::undo() const
    {
        auto p = piece.getPosition();
        piece.setPosition(p + offset);
    }


    CombinedTransformation::CombinedTransformation(Piece& piece) :
        PieceTransformation(piece)
    {
    }

    void CombinedTransformation::add(
            std::unique_ptr<PieceTransformation> transformation)
    {
        transformations.push_back(std::move(transformation));
    }

    void CombinedTransformation::execute() const
    {
        std::for_each(transformations.begin(),
                transformations.end(),
                [](const std::unique_ptr<PieceTransformation>& t){ t->execute(); });
    }

    void CombinedTransformation::undo() const
    {
        std::for_each(transformations.begin(),
                transformations.end(),
                [](const std::unique_ptr<PieceTransformation>& t){ t->undo(); });
    }


} /* namespace tetris */
