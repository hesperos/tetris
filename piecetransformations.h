#ifndef PIECETRANSFORMATIONS_H_
#define PIECETRANSFORMATIONS_H_

#include "command.h"
#include "piece.h"

#include <memory>
#include <vector>

namespace tetris
{
    class PieceTransformation :
        public Command
    {
    public:
        virtual ~PieceTransformation() = default;
        PieceTransformation(Piece& piece);

    protected:
        Piece& piece;
    };

    class PieceRotate :
        public PieceTransformation
    {
    public:
        PieceRotate(Piece& piece, int rotations = 1);

        void execute() const override;
        void undo() const override;

    protected:
        const int rotations;
    };

    class PieceShiftLeft :
        public PieceTransformation
    {
    public:
        PieceShiftLeft(Piece& piece, size_t offset = 1);

        void execute() const override;
        void undo() const override;

    protected:
        const size_t offset;
    };

    class PieceShiftRight :
        public PieceTransformation
    {
    public:
        PieceShiftRight(Piece& piece, size_t offset = 1);

        void execute() const override;
        void undo() const override;

    protected:
        const size_t offset;
    };

    class PieceMoveDown :
        public PieceTransformation
    {
    public:
        PieceMoveDown(Piece& piece, size_t offset = 1);

        void execute() const override;
        void undo() const override;

    protected:
        const size_t offset;
    };

    class PieceMoveUp :
        public PieceTransformation
    {
    public:
        PieceMoveUp(Piece& piece, size_t offset = 1);

        void execute() const override;
        void undo() const override;

    protected:
        const size_t offset;
    };

    class CombinedTransformation :
        public PieceTransformation
    {
    public:
        CombinedTransformation(Piece& piece);

        void add(std::unique_ptr<PieceTransformation> transformation);

        void execute() const override;
        void undo() const override;

    protected:
        std::vector<std::unique_ptr<PieceTransformation>> transformations;
    };

} /* namespace tetris */


#endif /* PIECETRANSFORMATIONS_H_ */
