#ifndef COLOR_H_
#define COLOR_H_

namespace tetris
{
    enum class Color : int
    {
        None = 0,

        Red,
        Green,
        Blue,
        Yellow,
        Magenta,
        Cyan,
        White,

        Last,
    };

} /* namespace tetris */

#endif /* COLOR_H_ */
