#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#include "types.h"

namespace tetris
{
    const Dimension BoardWidth = 10;
    const Dimension BoardHeight = 16;

    const Dimension GridWidth = 4;
    const Dimension GridHeight = 4;
    const Dimension GridRotations = 4;

    const int HorizontalBorder = 1;
    const int VerticalBorder = 1;

    constexpr int HorizontalBorders = 2 * HorizontalBorder;
    constexpr int VerticalBorders = 2 * VerticalBorder;
} /* namespace tetris */

#endif /* CONSTANTS_H_ */
