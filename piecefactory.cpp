#include "piecefactory.h"
#include "board.h"

#include <boost/make_unique.hpp>

namespace tetris
{
    namespace
    {
        const PieceGrid pieceGridL {
                "0000"
                "0010"
                "0010"
                "0110"

                "0000"
                "1000"
                "1110"
                "0000"

                "0110"
                "0100"
                "0100"
                "0000"

                "0000"
                "0111"
                "0001"
                "0000"
        };

        const PieceGrid pieceGridIL {
                "0000"
                "0100"
                "0100"
                "0110"

                "0000"
                "1110"
                "1000"
                "0000"

                "0110"
                "0010"
                "0010"
                "0000"

                "0000"
                "0001"
                "0111"
                "0000"
        };

        const PieceGrid pieceGridI {
                "0010"
                "0010"
                "0010"
                "0010"

                "0000"
                "0000"
                "1111"
                "0000"

                "0010"
                "0010"
                "0010"
                "0010"

                "0000"
                "0000"
                "1111"
                "0000"
        };

        const PieceGrid pieceGridS {
                "0000"
                "0110"
                "0011"
                "0000"

                "0000"
                "0010"
                "0110"
                "0100"

                "0000"
                "0110"
                "0011"
                "0000"

                "0000"
                "0010"
                "0110"
                "0100"
        };

        const PieceGrid pieceGridZ {
                "0000"
                "0110"
                "1100"
                "0000"

                "0000"
                "0100"
                "0110"
                "0010"

                "0000"
                "0110"
                "1100"
                "0000"

                "0000"
                "0100"
                "0110"
                "0010"
        };

        const PieceGrid pieceGridO {
                "0000"
                "0110"
                "0110"
                "0000"

                "0000"
                "0110"
                "0110"
                "0000"

                "0000"
                "0110"
                "0110"
                "0000"

                "0000"
                "0110"
                "0110"
                "0000"
        };

        const PieceGrid pieceGridE {
                "0000"
                "0010"
                "0111"
                "0000"

                "0000"
                "0001"
                "0011"
                "0001"

                "0000"
                "0111"
                "0010"
                "0000"

                "0000"
                "0100"
                "0110"
                "0100"
        };
    } /* namespace */

    std::vector<PieceGrid> PieceFactory::grids = {
        pieceGridL,
        pieceGridIL,
        pieceGridI,
        pieceGridS,
        pieceGridZ,
        pieceGridO,
        pieceGridE,
    };

    PieceFactory::PieceFactory() :
        randomGenerator{randomDevice()},
        randomGrid{0, grids.size() - 1},
        randomRotation{0, PieceGrid::Rotations - 1},
        randomColor{static_cast<int>(Color::None) + 1,
            static_cast<int>(Color::Last) - 1},
        randomHorizontalPos{0, TetrisBoard::Width - 1}
    {
    }

    std::unique_ptr<Piece> PieceFactory::createRandom(const Speed& speed)
    {
        return boost::make_unique<Piece>(
                grids.at(randomGrid(randomGenerator)),
                BoardPosition(randomHorizontalPos(randomGenerator)),
                randomRotation(randomGenerator),
                speed,
                static_cast<Color>(randomColor(randomGenerator)));
    }
} /* namespace tetris */

