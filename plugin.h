#ifndef PLUGIN_H_
#define PLUGIN_H_

namespace tetris
{
    class Plugin
    {
    public:
        virtual ~Plugin() = default;
    };
} /* namespace tetris */

#endif /* PLUGIN_H_ */
