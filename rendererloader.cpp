#include "rendererloader.h"

namespace tetris
{
    RendererLoader::RendererLoader(const std::string& rendererName) :
        pluginLoader("renderer_" + rendererName)
    {
    }

} /* namespace tetris */
