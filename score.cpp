#include "score.h"

#include <boost/bind.hpp>

namespace tetris
{
    Score::Score()
    {
        reset();
    }

    int Score::get() const
    {
        return score_;
    }

    void Score::reset()
    {
        score_ = 0;
    }

    void Score::score(const Level& level, int linesDeleted)
    {
        const int multipliers[] = { 40, 100, 300, 1200 };
        const int l = level.get();

        for (int nLines = 4; nLines>0; --nLines)
        {
            const int lines = linesDeleted / nLines;
            linesDeleted = linesDeleted % nLines;
            if (lines)
            {
                score_ += multipliers[nLines - 1] * (l + 1);
            }
        }
    }

    Level::Level()
    {
        reset();
    }

    int Level::get() const
    {
        return level;
    }

    void Level::reset()
    {
        level = 0;
    }

    void Level::up()
    {
        if (20 > level)
        {
            ++level;
            produceEvent(boost::bind(
                        &LevelListener::LevelChanged, _1, *this));
        }
    }

    GameScore::GameScore(std::unique_ptr<Level> level,
            std::unique_ptr<Score> score) :
        level(std::move(level)),
        score_(std::move(score))
    {
        reset();
    }

    void GameScore::score(int linesDeleted)
    {
        linesCleared += linesDeleted;
        if (linesCleared > 10)
        {
            level->up();
            linesCleared -= (linesCleared/10) * 10;
        }

        score_->score(*level, linesDeleted);

    }

    Speed GameScore::getSpeed() const
    {
        Speed framesPerRow = 3;
        switch(level->get())
        {
        case 0: framesPerRow = 53; break;
        case 1: framesPerRow = 49; break;
        case 2: framesPerRow = 45; break;
        case 3: framesPerRow = 41; break;
        case 4: framesPerRow = 37; break;
        case 5: framesPerRow = 33; break;
        case 6: framesPerRow = 28; break;
        case 7: framesPerRow = 22; break;
        case 8: framesPerRow = 17; break;
        case 9: framesPerRow = 11; break;
        case 10: framesPerRow = 10; break;
        case 11: framesPerRow = 9; break;
        case 12: framesPerRow = 8; break;
        case 13: framesPerRow = 7; break;
        case 14: framesPerRow = 6; break;
        case 15: framesPerRow = 6; break;
        case 16: framesPerRow = 5; break;
        case 17: framesPerRow = 5; break;
        case 18: framesPerRow = 4; break;
        case 19: framesPerRow = 4; break;
        case 20: framesPerRow = 3; break;
        default:
            break;
        }

        return framesPerRow;
    }

    void GameScore::reset()
    {
        level->reset();
        score_->reset();
        linesCleared = 0;
    }

    Score& GameScore::getScore() const
    {
        return *score_;
    }

    Level& GameScore::getLevel() const
    {
        return *level;
    }
} /* namespace tetris */
