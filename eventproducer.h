#ifndef _EVENTPRODUCER_H_
#define _EVENTPRODUCER_H_

#include <memory>
#include <set>

#include <boost/function.hpp>

namespace tetris
{

    template <typename EventListenerT>
        class EventProducer
    {
    public:
        void addListener(std::weak_ptr<EventListenerT> listener)
        {
            listeners.insert(listener);
        }

        void removeListener(std::weak_ptr<EventListenerT> listener)
        {
            auto it = listeners.find(listener);
            if (it != listeners.end())
            {
                listeners.erase(it);
            }
        }

    protected:
        template <typename FunctorT>
        void produceEvent(FunctorT&& event)
        {
            // signal all registered listeners
            for (const auto& listener : listeners)
            {
                if (std::shared_ptr<EventListenerT> strong = listener.lock())
                {
                    event(strong);
                }
            }
        }

        std::set<std::weak_ptr<EventListenerT>> listeners;
    };

} /* namespace tetris */

#endif /* _EVENTPRODUCER_H_ */
