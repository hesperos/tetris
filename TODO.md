# High priority
- Add tests
- Add game over screen (state)

# Medium priority
- Add signal handler
- Add profiling (google profiler?)

# Low priority
- Introduce configuration file
- Separate renderers into plugins
- Modify Renderer Factory to be a plugin loader
- Add SDL renderer
