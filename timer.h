#ifndef TIMER_H_
#define TIMER_H_

#include <memory>
#include <thread>

#include <boost/date_time/posix_time/posix_time.hpp>

namespace tetris
{
    class Timer
    {
    public:
        Timer(boost::posix_time::time_duration d,
            std::function<void()> callback);
        ~Timer();

        void stop();

    private:
        class TimerImpl;
        std::shared_ptr<TimerImpl> pImpl;
        std::thread ioThread;
    };

} /* namespace tetris */

#endif /* TIMER_H_ */
