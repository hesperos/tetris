# tetris

![Screenshot ncurses](screenshot.png)

![Screenshot sdl2](screenshot_sdl.png)

This is a simple implementation in C++11 of this classic game. Main
features of this implementation:

- Rendering engine is abstracted - that allows the implementation of any
rendering system without having to modify any of the game logic,
- Features a simple state machine to implement various game screens,
- The code is testable,

This implementation has been created as a learning exercise mostly.
