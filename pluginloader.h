#ifndef PLUGINLOADER_H_
#define PLUGINLOADER_H_

#include "library.h"
#include "plugin.h"

#include <memory>

namespace tetris
{
    class PluginLoader
    {
    public:
        PluginLoader(const std::string& pluginName);

        template <typename ... ArgsT>
            std::unique_ptr<Plugin> instantiate(ArgsT ... args) const
            {
                using PluginCreator = std::unique_ptr<Plugin>(*)(ArgsT...);
                PluginCreator c = pluginLibrary.getSymbol<PluginCreator>(
                        "create");
                return c(std::forward<ArgsT>(args)...);
            }

    protected:
        Library pluginLibrary;
    };


} /* namespace tetris */

#endif /* PLUGINLOADER_H_ */
