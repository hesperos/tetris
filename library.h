#ifndef LIBRARY_H_
#define LIBRARY_H_

#include <dlfcn.h>
#include <string>
#include <sstream>

namespace tetris
{
    class Library
    {
    public:
        virtual ~Library();
        Library(const std::string& libraryName,
                int flags = RTLD_LAZY,
                bool unloadRaii = false);

        template <typename SymbolT>
            SymbolT getSymbol(const std::string& symbol) const
            {
                return reinterpret_cast<SymbolT>(
                        dlsym(handle, symbol.c_str()));
            }

    protected:
        const std::string libraryName;
        const int flags;
        bool unloadRaii;
        void *handle;
    };

} /* namespace tetris */


#endif /* LIBRARY_H_ */
