#ifndef _TYPES_H_
#define _TYPES_H_

#include <cstddef>


namespace tetris
{
    using Speed = int;
    using Rotation = std::size_t;
    using Dimension = std::size_t;

} /* namespace tetris */


#endif /* _TYPES_H_ */
