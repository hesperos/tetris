#ifndef GAME_H_
#define GAME_H_

#include "fsm.h"
#include "renderer.h"

namespace tetris
{
    class Game :
        public Fsm
    {
    public:
        Game(const std::shared_ptr<Renderer>& renderer);

    protected:
        std::shared_ptr<Renderer> renderer;
        std::unique_ptr<State> makeState(int state) override;

    private:
        std::unique_ptr<State> createMenuState();
        std::unique_ptr<State> createTetrisState();
        std::unique_ptr<State> createHighScoresState();
    };
} /* namespace tetris */

#endif /* GAME_H_ */
