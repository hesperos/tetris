#ifndef _MOCKLEVEL_H_
#define _MOCKLEVEL_H_

#include <gmock/gmock.h>

#include "../../score.h"

class MockLevel : public tetris::Level
{
public:
    MOCK_CONST_METHOD0(get, int());
    MOCK_METHOD0(reset, void());
    MOCK_METHOD0(up, void());
};


#endif /* _MOCKLEVEL_H_ */
