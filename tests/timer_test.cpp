#include <gmock/gmock.h>
#include <gtest/gtest.h>

// mocks

// module under test
#include "../timer.h"

#include <chrono>
#include <memory>

#include <boost/make_unique.hpp>

using namespace tetris;
using namespace ::testing;

namespace
{
    class Counter
    {
    public:
        Counter() :
            c{0}
        {
        }

        void increment()
        {
            c++;
        }

        int get() const
        {
            return c;
        }

    protected:
        int c;
    };
} // namespace

class TimerTest :
    public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }

protected:
};

TEST_F(TimerTest, test_ifFiresCallbackInProgrammedIntervals)
{
    const int n = 5;

    for (int interval = 50; interval < 250; interval += 50)
    {
        std::unique_ptr<Counter> counter
            = boost::make_unique<Counter>();

        {
            std::unique_ptr<Timer> timer =
                boost::make_unique<Timer>(boost::posix_time::milliseconds{interval},
                        std::bind(&Counter::increment, counter.get()));

            std::this_thread::sleep_for(
                    std::chrono::milliseconds(n * interval));
        }

        ASSERT_NEAR(counter->get(), n, 1);
    }
}
