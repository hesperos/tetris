#include <gmock/gmock.h>
#include <gtest/gtest.h>

// mocks
#include "mock/MockLevel.h"

// module under test
#include "../score.h"

#include <algorithm>
#include <numeric>

#include <boost/make_unique.hpp>

using namespace tetris;
using namespace ::testing;

namespace
{
    class TestableScore : public Score
    {
    public:
        using Score::Score;
        void set(int score)
        {
            score_ = score;
        }
    };
}

class ScoreTest :
    public ::testing::Test
{
public:
    void SetUp()
    {
        mockLevel = boost::make_unique<MockLevel>();
        score = boost::make_unique<TestableScore>();
    }

    void TearDown()
    {
    }

protected:
    std::unique_ptr<TestableScore> score;
    std::unique_ptr<MockLevel> mockLevel;
};

TEST_F(ScoreTest, test_ifResetsToZeroOnConstruction)
{
    ASSERT_EQ(0, score->get());
}

TEST_F(ScoreTest, test_ifResetRestoresZero)
{
    score->set(123);
    ASSERT_NE(0, score->get());

    score->reset();
    ASSERT_EQ(0, score->get());
}
    
TEST_F(ScoreTest, test_ifScoreIsCumulative)
{
    EXPECT_CALL(*mockLevel, get())
        .WillOnce(Return(7))
        .WillOnce(Return(3))
        .WillOnce(Return(7))        
        .WillOnce(Return(3));

    score->reset();
    score->score(*mockLevel, 3);
    int first = score->get();

    score->reset();
    score->score(*mockLevel, 1);
    int second = score->get();

    score->reset();
    score->score(*mockLevel, 3);
    score->score(*mockLevel, 1);    

    ASSERT_EQ(first + second, score->get());
}

TEST_F(ScoreTest, test_ifCalculatesScoreCorrectly)
{
    const int n = 4;
    const int l = 21;
    
    std::vector<int> levels(l);
    std::iota(levels.begin(), levels.end(), 0);

    InSequence inSequence{};
    
    for (int nLines = 0; nLines <= n; nLines++)
    {
        for (const auto& level : levels)            
        {
            EXPECT_CALL(*mockLevel, get())
                .WillOnce(Return(level))
                .RetiresOnSaturation();
        }
    }
    
    for (int nLines = 0; nLines <= n; nLines++)
    {
        for (const auto& level : levels)
        {
            score->reset();
            score->score(*mockLevel, nLines);

            int expected = 0;
            if (0 < nLines)
            {
                const int multipliers[] = { 40, 100, 300, 1200 };
                expected = multipliers[nLines - 1] * (level + 1);
            }
            
            ASSERT_EQ(expected, score->get());
        }
    }
 }
