#include <gmock/gmock.h>
#include <gtest/gtest.h>

// mocks

// module under test
#include "../position.h"

#include <memory>

#include <boost/make_unique.hpp>

using namespace tetris;
using namespace ::testing;

class PositionTest :
    public ::testing::Test
{
public:
    void SetUp()
    {
    }

    void TearDown()
    {
    }

protected:
};

TEST_F(PositionTest, test_ifIsDefaultConstructible)
{
    boost::make_unique<BoardPosition>();
}

TEST_F(PositionTest, test_ifShiftingWorks)
{
    const int initX = 1;

    std::unique_ptr<BoardPosition> p =
        boost::make_unique<BoardPosition>(initX);

    ASSERT_EQ(initX - 1, (*p << 1).getX());
    ASSERT_EQ(initX, (*p >> 1).getX());
}

TEST_F(PositionTest, test_ifVerticalShiftingWorks)
{
    const int initY = 1;

    std::unique_ptr<BoardPosition> p =
        boost::make_unique<BoardPosition>(0, initY);

    ASSERT_EQ(initY + 1, (*p + 1).getY());
    ASSERT_EQ(initY, (*p - 1).getY());
}
