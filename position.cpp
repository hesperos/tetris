#include "position.h"

namespace tetris
{
    BoardPosition::BoardPosition(uint8_t x, uint8_t y) :
        position({ x, y })
    {
    }

    BoardPosition& BoardPosition::operator<<(uint8_t n)
    {
        position.x -= n;
        return *this;
    }

    BoardPosition& BoardPosition::operator>>(uint8_t n)
    {
        position.x += n;
        return *this;
    }

    BoardPosition& BoardPosition::operator+(uint8_t n)
    {
        position.y += n;
        return *this;
    }

    BoardPosition& BoardPosition::operator-(uint8_t n)
    {
        position.y -= n;
        return *this;
    }

    int8_t BoardPosition::getX() const
    {
        return position.x;
    }

    int8_t BoardPosition::getY() const
    {
        return position.y;
    }

} /* namespace tetris */

