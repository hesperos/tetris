#ifndef PIECE_H_
#define PIECE_H_

#include "color.h"
#include "constants.h"
#include "grid.h"
#include "paintable.h"
#include "position.h"
#include "types.h"


namespace tetris
{
    using PieceGrid = Grid<GridWidth, GridHeight, GridRotations>;

    class Piece :
        public Paintable<Piece>
    {
    public:
        virtual ~Piece() = default;

        Piece(PieceGrid pieceGrid,
                BoardPosition position,
                Rotation rotation,
                Speed speed,
                Color color = Color::Red);

        void rotate(Rotation n = 1);
        void setPosition(const BoardPosition& p);

        const PieceGrid& getGrid() const;
        const BoardPosition& getPosition() const;
        Color getColor() const;

        Speed getSpeed() const;
        void setSpeed(const Speed& speed);

    protected:
        PieceGrid grid;
        BoardPosition position;
        Speed speed;
        const Color color;
    };

} /* namespace tetris */

#endif /* PIECE_H_ */
