#ifndef TETRISSTATE_H_
#define TETRISSTATE_H_

#include "board.h"
#include "elementpainter.h"
#include "fsm.h"
#include "piecefactory.h"
#include "renderer.h"
#include "score.h"
#include "tetris.h"
#include "timer.h"

#include <boost/date_time/posix_time/posix_time.hpp>

namespace tetris
{
    class TetrisState :
        public State
    {
    public:
        TetrisState(std::unique_ptr<TetrisBoard> board,
                std::unique_ptr<PieceFactory> pf,
                std::unique_ptr<ElementPainter> ep,
                std::unique_ptr<GameScore> score,
                const std::shared_ptr<Renderer>& renderer,
                boost::posix_time::time_duration refreshRate);

        boost::optional<Id> enter(const Fsm& fsm) override;

    private:
        std::unique_ptr<Tetris> tetris;
        std::unique_ptr<Timer> timer;
        std::shared_ptr<Renderer> renderer;
    };
} /* namespace tetris */

#endif /* TETRISSTATE_H_ */
