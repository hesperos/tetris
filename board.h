#ifndef BOARD_H_
#define BOARD_H_

#include <algorithm>
#include <array>
#include <functional>
#include <cstddef>

#include "color.h"
#include "constants.h"
#include "paintable.h"
#include "piece.h"
#include "types.h"

namespace tetris
{
    struct BoundaryInfo
    {
        int left, right, bottom;

        bool any() const
        {
            return left || right || bottom;
        }
    };

    template <Dimension WidthV, Dimension HeightV>
        class Board :
            public Paintable<Board<WidthV,HeightV>>
        {
        public:
            using Grid = std::array<std::array<Color, WidthV>, HeightV>;
            constexpr static Dimension Width = WidthV;
            constexpr static Dimension Height = HeightV;

            virtual ~Board() = default;

            bool isColliding(
                    const PieceGrid& grid,
                    const BoardPosition& position) const;

            bool isColliding(const Piece& piece) const;

            BoundaryInfo isOutside(
                    const PieceGrid& grid,
                    const BoardPosition& position) const;

            BoundaryInfo isOutside(const Piece& piece) const;

            void meltIn(std::unique_ptr<Piece> piece);

            Color index(Dimension x, Dimension y) const;

            int clearCompleteLines();

        private:
            Grid grid;
        };

    using TetrisBoard = Board<BoardWidth, BoardHeight>;

    template <Dimension WidthV, Dimension HeightV>
        bool Board<WidthV, HeightV>::isColliding(
                const PieceGrid& grid,
                const BoardPosition& position) const
        {
            for (Dimension i = 0; i < PieceGrid::Width; ++i)
            {
                for (Dimension j = 0; j < PieceGrid::Height; ++j)
                {
                    int topX = i + position.getX();
                    int topY = j + position.getY();

                    if (!grid(i, j))
                    {
                        continue;
                    }

                    if (Color::None != this->grid[topY][topX])
                    {
                        return true;
                    }
                }
            }
            return false;
        }

    template <Dimension WidthV, Dimension HeightV>
        bool Board<WidthV, HeightV>::isColliding(const Piece& piece) const
        {
            return isColliding(piece.getGrid(), piece.getPosition());
        }

    template <Dimension WidthV, Dimension HeightV>
        BoundaryInfo Board<WidthV, HeightV>::isOutside(
                const PieceGrid& grid,
                const BoardPosition& position) const
        {
            BoundaryInfo b{ 0, 0, 0 };
            for (Dimension i = 0; i < PieceGrid::Width; ++i)
            {
                for (Dimension j = 0; j < PieceGrid::Height; ++j)
                {
                    int topX = i + position.getX();
                    int topY = j + position.getY();

                    if (!grid(i, j))
                    {
                        continue;
                    }

                    if (topX < 0)
                        b.left = std::max(std::abs(topX), b.left);
                    else if (topX >= Width)
                        b.right = std::max(topX - static_cast<int>(Width-1),
                                b.right);

                    if (topY >= Height)
                    {
                        b.bottom = std::max(topY - static_cast<int>(Height-1),
                                b.bottom);
                    }
                }
            }
            return b;
        }

    template <Dimension WidthV, Dimension HeightV>
        BoundaryInfo Board<WidthV, HeightV>::isOutside(const Piece& piece) const
        {
            return isOutside(piece.getGrid(), piece.getPosition());
        }

    template <Dimension WidthV, Dimension HeightV>
        void Board<WidthV, HeightV>::meltIn(std::unique_ptr<Piece> piece)
        {
            const BoardPosition& position = piece->getPosition();
            for (Dimension i = 0; i < PieceGrid::Width; ++i)
            {
                for (Dimension j = 0; j < PieceGrid::Height; ++j)
                {
                    int topX = i + position.getX();
                    int topY = j + position.getY();

                    if (!piece->getGrid()(i, j))
                    {
                        continue;
                    }

                    this->grid[topY][topX] = piece->getColor();
                }
            }
        }

    template <Dimension WidthV, Dimension HeightV>
        Color Board<WidthV, HeightV>::index(Dimension x, Dimension y) const
        {
            return grid[y][x];
        }

    template <Dimension WidthV, Dimension HeightV>
        int Board<WidthV, HeightV>::clearCompleteLines()
        {
            int completeLines = 0;
            for (int m = 0; m < grid.size(); m++)
            {
                if (std::find(std::begin(grid[m]),
                            std::end(grid[m]),
                            Color::None) == std::end(grid[m]))
                {
                    ++completeLines;
                    for (int z = m; z > 0; z--)
                    {
                        grid[z] = grid[z - 1];
                    }
                }
            }
            return completeLines;
        }

} /* namespace tetris */

#endif /* BOARD_H_ */
