#include "gamestates.h"
#include "tetrisstate.h"

#include <boost/make_unique.hpp>

namespace tetris
{

    TetrisState::TetrisState(std::unique_ptr<TetrisBoard> board,
                std::unique_ptr<PieceFactory> pf,
                std::unique_ptr<ElementPainter> ep,
                std::unique_ptr<GameScore> score,
                const std::shared_ptr<Renderer>& renderer,
                boost::posix_time::time_duration refreshRate) :
        tetris(boost::make_unique<Tetris>(std::move(board),
                    std::move(pf),
                    std::move(ep),
                    std::move(score),
                    renderer)),
        timer(boost::make_unique<Timer>(refreshRate,
                    std::bind(&Tetris::step, tetris.get()))),
        renderer(renderer)
    {
    }

    boost::optional<State::Id> TetrisState::enter(const Fsm& fsm)
    {
        while (tetris->isRunning())
        {
            // slowly poll
            std::this_thread::sleep_for(std::chrono::milliseconds(250));
        }

        timer->stop();
        return static_cast<Id>(GameState::Menu);
    }

} /* namespace tetris */
