#include "board.h"
#include "piecetransformations.h"
#include "tetris.h"

#include <boost/make_unique.hpp>

namespace tetris
{
    Tetris::Tetris(std::unique_ptr<TetrisBoard> board,
            std::unique_ptr<PieceFactory> pf,
            std::unique_ptr<ElementPainter> ep,
            std::unique_ptr<GameScore> score,
            const std::shared_ptr<Renderer>& renderer) :
        board(std::move(board)),
        pieceFactory(std::move(pf)),
        elementPainter(std::move(ep)),
        score(std::move(score)),
        renderer(renderer),
        isRunning_(true),
        isPieceRepaintRequired(false),
        isBoardRepaintRequired(true),
        isClearBufferNeeded(true),
        frameCounter(0)
    {
    }

    bool Tetris::isRunning() const
    {
        return isRunning_;
    }

    void Tetris::step()
    {
        if (!currentPiece)
        {
            currentPiece = pieceFactory->createRandom(
                    score->getSpeed());

            isRunning_ = !adjustPieceToBoard();
            isPieceRepaintRequired = true;
        }

        processInput();
        progressPiece();

        if (int linesCleared = board->clearCompleteLines())
        {
            score->score(linesCleared);
            isBoardRepaintRequired = true;
            isClearBufferNeeded = true;
        }

        render();
    }

    bool Tetris::detectCollisions()
    {
        if (board->isColliding(*currentPiece))
        {
            return true;
        }
        else
        {
            return adjustPieceToBoard();
        }

        return false;
    }

    bool Tetris::isForbidden()
    {
        // TODO implement me
        return false;
    }

    void Tetris::progressPiece()
    {
        if (++frameCounter % currentPiece->getSpeed())
        {
            return;
        }

        PieceMoveDown moveDown{*currentPiece};
        moveDown.execute();

        if (detectCollisions())
        {
            moveDown.undo();
            board->meltIn(std::move(currentPiece));
            isBoardRepaintRequired = true;
        }

        isPieceRepaintRequired = true;
    }

    bool Tetris::adjustPieceToBoard()
    {
        const BoundaryInfo& b = board->isOutside(currentPiece->getGrid(),
                    currentPiece->getPosition());

        CombinedTransformation ct{*currentPiece};

        if (b.left)
        {
            ct.add(boost::make_unique<PieceShiftRight>(
                        *currentPiece, b.left));
        }

        if (b.right)
        {
            ct.add(boost::make_unique<PieceShiftLeft>(
                        *currentPiece, b.right));
        }

        ct.execute();
        if (board->isColliding(*currentPiece) || b.bottom)
        {
            ct.undo();
            return true;
        }

        return false;
    }

    void Tetris::processInput()
    {
        const boost::optional<Renderer::Input>& input =
            renderer->getKey();

        if (!input)
        {
            return;
        }

        std::unique_ptr<PieceTransformation> transformation;
        switch(*input)
        {
            case Renderer::Input::Quit:
                isRunning_ = false;
                break;

            case Renderer::Input::Left:
                transformation =
                    boost::make_unique<PieceShiftLeft>(*currentPiece);
                break;

            case Renderer::Input::Right:
                transformation =
                    boost::make_unique<PieceShiftRight>(*currentPiece);
                break;

            case Renderer::Input::Up:
                transformation =
                    boost::make_unique<PieceRotate>(*currentPiece);
                break;

            case Renderer::Input::Down:
                currentPiece->setSpeed(
                        currentPiece->getSpeed() / 12);
                break;

            default:
                break;
        } // switch

        if (transformation)
        {
            transformation->execute();
            if (detectCollisions() || isForbidden())
            {
                transformation->undo();
            }
            else
            {
                isPieceRepaintRequired = true;
            }
        } // if (transformation)
    }

    void Tetris::render()
    {
        if (!isClearBufferNeeded &&
                !isPieceRepaintRequired &&
                !isBoardRepaintRequired)
        {
            return;
        }

        bool isChanged = false;

        if (isClearBufferNeeded)
        {
            elementPainter->clear();
            isClearBufferNeeded = false;
        }

        if (isBoardRepaintRequired)
        {
            elementPainter->paint(*board);
            isBoardRepaintRequired = false;
            isChanged = true;
        }

        if (currentPiece && isPieceRepaintRequired)
        {
            elementPainter->paint(*currentPiece);
            isPieceRepaintRequired = false;
            isChanged = true;
        }

        if (isChanged)
        {
            elementPainter->paint(*score);
            elementPainter->blit();
        }
    }

} /* namespace tetris */
