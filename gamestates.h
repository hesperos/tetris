#ifndef GAMESTATES_H_
#define GAMESTATES_H_

enum class GameState : int
{
    Menu,
    Game,
    HighScores,
};

#endif /* GAMESTATES_H_ */
