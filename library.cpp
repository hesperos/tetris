#include "library.h"

namespace tetris
{
    Library::Library(const std::string& libraryName_,
            int flags,
            bool unloadRaii) :
        libraryName("lib" + libraryName_ + ".so"),
        flags(flags),
        unloadRaii(unloadRaii),
        handle(dlopen(libraryName.c_str(), flags))
    {
        if (!handle)
        {
            std::stringstream ss;
            ss << "Unable to open library: "
                << libraryName
                << ", "
                << dlerror();
            throw std::runtime_error(ss.str());
        }
    }

    Library::~Library()
    {
        if (handle && unloadRaii)
        {
            dlclose(handle);
        }
    }

} /* namespace tetris */
